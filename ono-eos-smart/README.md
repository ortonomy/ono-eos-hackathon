
## Run node:
nodeos -e -p eosio --plugin eosio::chain_api_plugin --plugin eosio::history_api_plugin --max-irreversible-block-age 10000000 --http-server-address=0.0.0.0:8888

## Keys:
PW5K2AcfaJcnzQgtd7ccFiA7kR9CUbne2nJNjBCtXyKNb9DL7ZBgM  
Private key: 5JrsF5F63BMoTcJwBo7is4t4XPRpPbDyn8en9p7pzgfAJY74HkF  
Public key: EOS6vA1Gn5up8uiCRppeG4nVX2AN6egkhnJgdu9TmGo1YkCaKvEYJ  
Private key: 5JU5YYSFYgZGHZNhRnjeDgu7VpC6frdzFqz2JtE6f1TngPodYSF  
Public key: EOS8ep6TBcJXQxxTVbYLiGN311XUG57f43shi5oEGLDSwxtMEz5XL  

## GET BALANCE:
cleos get currency balance eosio.token seller ONO

## Wallet Unlock:
cleos wallet unlock -n PK

## Clean the data:
/Users/pavelkrolevets/Library/Application Support/eosio/nodeos/data

## Host:
alias cleos='cleos --url http://192.168.1.193:8888/'

## RUN:
cleos wallet create -n PK

cleos wallet import -n PK 5JrsF5F63BMoTcJwBo7is4t4XPRpPbDyn8en9p7pzgfAJY74HkF

cleos wallet import -n PK 5JU5YYSFYgZGHZNhRnjeDgu7VpC6frdzFqz2JtE6f1TngPodYSF

cleos create account eosio seller EOS6vA1Gn5up8uiCRppeG4nVX2AN6egkhnJgdu9TmGo1YkCaKvEYJ EOS8ep6TBcJXQxxTVbYLiGN311XUG57f43shi5oEGLDSwxtMEz5XL

cleos create account eosio buyer EOS6vA1Gn5up8uiCRppeG4nVX2AN6egkhnJgdu9TmGo1YkCaKvEYJ EOS8ep6TBcJXQxxTVbYLiGN311XUG57f43shi5oEGLDSwxtMEz5XL

cleos create account eosio eosio.token EOS6vA1Gn5up8uiCRppeG4nVX2AN6egkhnJgdu9TmGo1YkCaKvEYJ EOS8ep6TBcJXQxxTVbYLiGN311XUG57f43shi5oEGLDSwxtMEz5XL

cleos set contract eosio.token /Users/pavelkrolevets/eos/build/contracts/eosio.token/ -p eosio.token

cleos push action eosio.token create '[ "eosio", "100000.00 ONO"]' -p eosio.token

cleos push action eosio.token issue '[ "seller", "100.00 ONO", "memo" ]' -p eosio

cleos create account eosio esceos EOS6vA1Gn5up8uiCRppeG4nVX2AN6egkhnJgdu9TmGo1YkCaKvEYJ EOS8ep6TBcJXQxxTVbYLiGN311XUG57f43shi5oEGLDSwxtMEz5XL

cleos set contract esceos /Users/pavelkrolevets/eos/build/contracts/esceos/ -p esceos

cleos push action eosio updateauth '{"account":"seller","permission":"active","parent":"owner","auth":{"keys":[{"key":"EOS6vA1Gn5up8uiCRppeG4nVX2AN6egkhnJgdu9TmGo1YkCaKvEYJ", "weight":1}],"threshold":1,"accounts":[{"permission":{"actor":"esceos","permission":"eosio.code"},"weight":1}],"waits":[]}}' -p seller

cleos push action eosio updateauth '{"account":"esceos","permission":"active","parent":"owner","auth":{"keys":[{"key":"EOS6vA1Gn5up8uiCRppeG4nVX2AN6egkhnJgdu9TmGo1YkCaKvEYJ", "weight":1}],"threshold":1,"accounts":[{"permission":{"actor":"esceos","permission":"eosio.code"},"weight":1}],"waits":[]}}' -p esceos

cleos push action esceos lock '["buyer", "seller", "4.00 ONO"]' -p seller

cleos push action esceos buyerok '["buyer", "seller"]' -p buyer

cleos push action esceos sellerok '["seller", "buyer"]' -p seller
