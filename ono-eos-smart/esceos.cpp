#include <eosiolib/eosio.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/contract.hpp>

using namespace eosio;

class escrow : public eosio::contract {
    public:
        using contract::contract;

        /// @abi action
        void lock( account_name buyer, account_name seller, const asset& quantity ) {
            require_auth( seller );
            require_recipient( N(buyer), N(seller) );
            eosio_assert( quantity.amount > 0, "invalid amount" );
            // TODO: add more validation here
            state_type state( _self, _self );

            state.emplace( _self, [&]( auto& record ) {
                record.id = state.available_primary_key();
                record.quantity = quantity;
                record.buyer = buyer;
                record.seller = seller;
                record.is_controversial = false;
                record.buyer_ok = false;
                record.seller_ok = false;
                record.time = now();
            });

            hold( seller, quantity );
        }

        /// @abi action
        void buyerok( account_name buyer, account_name seller ) {
            require_auth( buyer );
            require_recipient( N(buyer), N(seller) );
            state_type state( _self, _self );

            // TODO: more effective search
            for ( auto i = state.begin(); i != state.end(); ++i ) {
                if ( ( i->buyer == buyer ) && ( i->seller == seller ) && ( !i->buyer_ok ) ) {
                    state.modify( i, _self, [&]( auto& record ) {
                        record.buyer_ok = true;
                    } );

                    if ( i->buyer_ok && i->seller_ok ) release( i->buyer, i->quantity );
                }
            }
        }

        /// @abi action
        void sellerok( account_name seller, account_name buyer ) {
            require_auth( seller );
            require_recipient( N(buyer), N(seller) );
            state_type state( _self, _self );

            // TODO: more effective search
            for ( auto i = state.begin(); i != state.end(); ++i ) {
                if ( ( i->seller == seller ) && ( i->buyer == buyer ) && ( !i->seller_ok ) ) {
                    state.modify( i, _self, [&]( auto& record ) {
                        record.seller_ok = true;
                    } );

                    if ( i->buyer_ok && i->seller_ok ) release( i->buyer, i->quantity );
                }
            }
        }

        /// @abi action
        void sellerproblem( account_name seller, account_name buyer) {
            require_auth( seller );
            state_type state( _self, _self );

            // for ( auto i = state.begin(); i != state.end(); ++i ) {
            //     if ( ( i->seller == seller ) && ( i->buyer == buyer ) ) {
            //         state.modify( i, _self, [&]( auto& record ) {
            //             record.despute = true;
            //         } );
            //     }
            // }

            // select arbiter (arbiter table)
            // save arbiter public key  to state

            // notify arbiter
        }

        /// @abi action
        void buyerproblem() {
            // mark as problem
            // select arbiter
            // save arbiter public key  to state
            // notify arbiter
        }

        /// @abi action
        void showstate() {
            // find problem transfer by arbiter id
            // show deal info to arbiter
        }

        /// @abi action
        void buyerwin() {
            // crypto (contract) -> buyer
        }

        /// @abi action
        void sellerwin() {
            // crypto (insurance fund) -> seller
        }


    private:
        /// @abi table state i64
        struct state {
            uint64_t id;
            asset quantity;
            account_name buyer;
            account_name seller;
            bool is_controversial;
            bool buyer_ok;
            bool seller_ok;
            uint64_t time;

            auto primary_key() const { return id; }

            EOSLIB_SERIALIZE( state,
                (id)(quantity)(buyer)(seller)(is_controversial)(buyer_ok)(seller_ok) )
        };

        // TODO: remake as in examples
        using state_type = multi_index<N(state), state>;

        // /// @abi table arbitrator i64
        // struct arbitrator {
        //     uint64_t id;
        //     uint64_t require_consideration;
        //     account_name arbitrator_account;
        //
        //     auto primary_key() const { return id; }
        //
        //     EOSLIB_SERIALIZE( arbitrator,
        //         (id)(arbitrator_account)(require_consideration) )
        // };
        //
        // // TODO: remake as in examples
        // using arbitrator_type = multi_index<N(arbitrator), arbitrator>;

        void hold( account_name seller, const asset& quantity ) {
            action(
                permission_level{ seller, N(active) },
                N(eosio.token), N(transfer),
                std::make_tuple(seller, get_self(), quantity, std::string("hold"))
            ).send();
        }

        void release( account_name buyer, const asset& quantity) {
            action(
                permission_level{ get_self(), N(active) },
                N(eosio.token), N(transfer),
                std::make_tuple(get_self(), buyer, quantity, std::string("release"))
            ).send();
        }
};

EOSIO_ABI( escrow, (lock)(buyerok)(sellerok) )
