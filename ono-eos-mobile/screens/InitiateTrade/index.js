import React, { Component } from 'react';
import { Text } from 'react-native';
import { SafeAreaView } from 'react-navigation';

import TradeSummary from '../../components/TradeSummary';
import TradeParametersController from '../../components/TradeParametersController';

export default class InitiateTrade extends Component {
  constructor(props) {
    super(props);
    this.goToChat = this.goToChat.bind(this);
  }

  goToChat() {
    this.props.navigation.navigate('ChatCore', {
      mode: this.props.navigation.getParam('mode'), 
      partner: this.props.navigation.getParam('user'), 
      self: this.props.navigation.getParam('self') 
    });
  }

  render () {
    return (
      <SafeAreaView
        style={{
          flex: 1
        }}
      >
        <TradeSummary
          mode={this.props.navigation.getParam('mode')}
          user={this.props.navigation.getParam('user')}
        />
        <TradeParametersController 
          user={this.props.navigation.getParam('user')}
          self={this.props.navigation.getParam('self')}
          goToChat={this.goToChat}
        />
      </SafeAreaView>
    )
  }
}