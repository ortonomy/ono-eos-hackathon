// library dependencies
import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, Button, ImageBackground, Image, Dimensions, View } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Axios from 'axios';


import WalletDetails from '../../components/WalletDetails';
import ExchangeRate  from '../../components/ExchangeRate';
import TradesListSwitcher from '../../components/TradesListSwitcher';
import TradeItem from '../../components/TradeItem';

const fakeUsers = {
  buy: {
    name: 'Sasha',
    trades: 0,
    offer: 150,
    img: 'https://thumb.ibb.co/byeFk8/sasha.png'
  },
  sell: {
    name: 'Nick',
    trades: 528,
    offer: 30,
    img: 'https://thumb.ibb.co/hiMZv8/nick.png'
  }
}

class TradesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      walletBalance: null,
      mode: 'sell'
    }
    this.handlePress = this.handlePress.bind(this);
    this.goToTrade = this.goToTrade.bind(this);
  }

  componentDidMount() {
    const url = `http://10.101.2.146:3000/balance`;
    console.log(url);
    Axios.post( url,
      {
        name: 'seller'
      }
    )
    .then ( result => {
      console.log('balance', result);
      this.setState({ walletBalance: result.data[0] })
    })
    .catch( e => {
      console.log(e);
    })
  }

  handlePress(e) {
    this.setState( pS => {
      return {
        mode: pS.mode === 'sell' ? 'buy' : 'sell'
      }
    })
  }

  goToTrade(mode) {
    this.props.navigation.navigate('InitiateTrade', { 
      mode: mode, 
      balance: this.state.walletBalance, 
      user: fakeUsers[mode==='sell' ? 'buy':'sell'],
      self: fakeUsers[mode==='sell' ? 'sell': 'buy']
    });
  }

  render() {
    
    return (
      <SafeAreaView
        style={{ flex: 1, flexDirection: 'column', alignItems: 'center', backgroundColor: '#FFF' }}
      >
        <WalletDetails balance={this.state.walletBalance} />
        <ExchangeRate balance={this.state.walletBalance} />
        <TradesListSwitcher
          handlePress={this.handlePress}
          mode={this.state.mode}
        />
        <TradeItem 
          mode={this.state.mode} 
          users={fakeUsers}
          goToTrade={this.goToTrade}
        />
      </SafeAreaView>
    )
  }
}


export default TradesList;