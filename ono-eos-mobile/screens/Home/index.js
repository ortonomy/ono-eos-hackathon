// library dependencies
import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, Button, ImageBackground, Image, Dimensions, View, TouchableHighlight, TouchableOpacity, Alert, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';

class Home extends Component {

  constructor(props) {
    super(props);

    this.onSuccessSecurity = this.onSuccessSecurity.bind(this);
    this.onFailSecurity = this.onFailSecurity.bind(this);
    this.onCancelSecurity = this.onCancelSecurity.bind(this);
  }

  state = {
    modalVisible: false,
  };


  
   
  onSuccessSecurity() {
    console.log("successsecurity");
    this.setState({modalVisible: false}); 
  }

  onFailSecurity() {
    this.setState({modalVisible: false});
    console.log("fail security");
  }
  onCancelSecurity() {
    this.setState({modalVisible: false});
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    // work with device dimensions for spacing
    const { height, width } = Dimensions.get('window');

    return (
      <SafeAreaView
        style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF' }}
      >
        <StatusBar
          barStyle="light-content"
        />
        <Image
          source={require('../../assets/images/main_logo.png')}
          style={{ height: height/8, width: height/8}}
        />
        <Text
          style={{
            fontSize: 18,
           // fontFamily: 'Menlo',
            color: '#4833C3',
            fontWeight: 'bold'
          }}
        >
          one-n-one
        </Text>
        
      </SafeAreaView>
    )
  }
}


export default Home;