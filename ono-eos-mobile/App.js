// library
import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator, SafeAreaView } from 'react-navigation';
import Button from 'react-native-button';
import { Avatar } from 'react-native-elements';

// screens
import Home from './screens/Home';
import TradesList from './screens/TradesList';
import InitiateTrade from './screens/InitiateTrade';
import ChatCore from './screens/ChatCore';

// core nav
const RootStack = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: ({navigation}) => ({
        headerTransparent: true,
        headerStyle: {
          shadowOpacity: 0,
          elevation: 0,
          borderBottomWidth: 0,
          shadowColor: 'transparent'
        },
        headerRight: (
          <Button
            containerStyle={{
              paddingRight: 20
            }}
            style={{
              letterSpacing: 5,
              fontWeight: '700'
            }}
            onPress={() => navigation.navigate('TradesList')}
          >
            SKIP
          </Button>
        )
      })
    },
    TradesList: {
      screen: TradesList,
      navigationOptions: ({navigation}) => ({
        headerStyle: {
          borderBottomWidth: 0,
          backgroundColor: '#FFF',
          shadowOpacity: 0,
          elevation: 0,
          shadowColor: 'transparent'
        },
        headerTitle: (

          <Text
            style={{
              color: '#ccc',
              letterSpacing: 8,
              fontSize: 16,
              fontWeight: '300'
            }}
          >
            { 'balance'.toUpperCase() }
          </Text>
        )
      }),
    },
    InitiateTrade: {
      screen: InitiateTrade,
      navigationOptions: ({navigation}) => ({
        headerTitle: (
          <Text
            style={{
              color: '#ccc',
              letterSpacing: 8,
              fontSize: 16,
              fontWeight: '300'
            }}
          >
            { 'trade amount'.toUpperCase() }
          </Text>
        ),
        headerStyle: {
          borderBottomWidth: 0,
          backgroundColor: '#FFF',
          shadowOpacity: 0,
          elevation: 0,
          shadowColor: 'transparent'
        }
      })
    },
    ChatCore: {
      screen: ChatCore,
      navigationOptions: ({navigation}) => ({
        headerTitle: (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingTop: 10,
              paddingBottom: 10
            }}
          >
            <Avatar
              small
              rounded
              source={{uri: navigation.getParam('partner').img}}
              activeOpacity={1.0}
            />
            <Text
              style={{
                color: '#000',
                fontSize: 18,
                fontWeight: '500',
                paddingLeft: 15,
                paddingRight: 15
              }}
            >
              { navigation.getParam('partner').name }
            </Text>
          </View>
        ),
        headerStyle: {
          borderBottomWidth: 0,
          backgroundColor: '#FFF',
          shadowOpacity: 0,
          elevation: 0,
          shadowColor: 'transparent'
        }
      })
    }
  },
  {
    initialRouteName: 'Home'
  }
);

export default class App extends React.Component {
  render() {
    return (
      <RootStack />
    );
  }
}
