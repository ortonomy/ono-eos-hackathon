import React, { Component } from 'react';
import { Text, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-navigation';

export default class WalletDetails extends Component {

  render() {
    return (
      <SafeAreaView
        style={{height: 48}}
      >
        <Text
          style={{
            fontSize: 34,
            fontWeight: '700'
          }}
        >
          { this.props.balance }
        </Text>
      </SafeAreaView>
    );
  }
}