import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { FormInput, FormLabel } from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Button from 'react-native-button';

export default class TradeParametersController extends Component {
  render() {
    const { height, width } = Dimensions.get('window');
    const { self, user } = this.props;
    console.log('offer', self.offer);
    return (
      <SafeAreaView
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          backgroundColor: '#FFF'
        }}
      >
        <KeyboardAwareScrollView 
          keyboardOpeningTime={ 200 } 
          contentContainerStyle={{ 
            flex: 1, 
            flexDirection: 'column', 
            alignItems: 'center', 
            justifyContent: 'center' 
          }}
        > 
          <SafeAreaView>
            <Text
              style={{
                fontSize: 22,
                fontWeight: '900',
                color: '#3A5AE4'
              }}
            >
              ¥ {user.offer}.00
            </Text>
          </SafeAreaView>
          <SafeAreaView
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              width: width * 0.8
            }}
          >
            <FormInput 
              keyboardType="numeric"
              value={`${self.offer}`}
              containerStyle={{
                width: width * 0.2,
                height: 50
              }}
              inputStyle={{
                color: '#000',
                fontSize: 34,
                fontWeight: '900'
              }}
            />
            <FormLabel
              labelStyle={{
                fontSize: 34,
                color: '#000',
                fontWeight: '900'
              }}
              containerStyle={{
                height: 80
              }}
            >
              ONO
            </FormLabel>
          </SafeAreaView>
          <SafeAreaView
            style={{
              flex: .5,
              flexDirection: 'column',
              justifyContent: 'center'
            }}
          >
            <Button
              containerStyle={{
                borderRadius: 30,
                backgroundColor: '#5A02D1',
                paddingLeft: 30,
                paddingRight: 30,
                paddingTop: 15,
                paddingBottom: 15
              }}
              style={{
                color: '#FFF',
                fontWeight: '900'
              }}
              onPress={()=>( this.props.goToChat() )}
            >
              Open trade chat
            </Button>
          </SafeAreaView>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    )
  }
}