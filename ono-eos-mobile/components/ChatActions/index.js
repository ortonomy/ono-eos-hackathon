import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import Button from 'react-native-button';
import { SafeAreaView } from 'react-navigation';

const status = {
  READY: 'READY',
  IN_ESCROW: 'IN_ESCROW',
  PAYMENT_SENT: 'PAYMENT_SENT',
  PAYMENT_RECEIVED: 'PAYMENT_RECEIVED'
}

export default class ChatActions extends Component {
  constructor(props) {
    super(props);

    this.handleAction = this.handleAction.bind(this);
    this.getButtonText = this.getButtonText.bind(this);
  }

  getButtonText(){
    console.log(this.props);
    if ( this.props.mode === 'sell' ){
      switch (this.props.status) {
        case 'READY': 
          return 'Fund escrow';
        case 'IN_ESCROW':
          return 'Wait for buyer'
        default:
          return 'something is broken';
      }
    } else {
      switch (this.props.status) {

      }
    }
  }

  handleAction() {
    switch (this.props.status) {
      case 'READY': 
        this.props.callbacks.READY();
      default:
        return 'something is broken';
    }
  }

  render () {
    const { height, width } = Dimensions.get('window');
    
    return (
      <SafeAreaView
        style={{
          height: 70,
          width: width,
          paddingTop: 20,
          paddingBottom: 20,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          shadowOffset:{  width: 0,  height: 5,  },
          shadowColor: '#999',
          shadowOpacity: .5,
        }}
      >
        <Button
          containerStyle={{
            backgroundColor: '#5A02D1',
            borderRadius: 30,
            paddingLeft: 30,
            paddingRight: 30,
            paddingTop: 15,
            height: 50
          }}
          style={{
            color: '#FFF',
            fontWeight: '900'
          }}
          onPress={()=>( this.handleAction() )}
        >
          { this.getButtonText() }
        </Button>
      </SafeAreaView>
    )
  }
}