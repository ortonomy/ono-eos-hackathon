import Express from 'express';
import bodyParser from 'body-parser';
import Eos from 'eosjs';
const { api } = Eos.modules;

const app = Express();

// EOS SETUP
const eosChainID = process.env['EOS_CHAIN_ID'] ||  'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f';
const eosHTTPProvider = process.env['EOS_NODE_HTTP'] || 'http://localhost:8888';
let lastKeyProvider = '5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3';
const getCurrentKeyProvider = () => {
  return lastKeyProvider;
};

const eosConfig = {
  chainId: eosChainID, // 32 byte (64 char) hex string
  keyProvider: getCurrentKeyProvider,
  httpEndpoint: eosHTTPProvider,
  expireInSeconds: 60,
  broadcast: true,
  debug: true, // API and transactions
  sign: true
};
const eos = Eos(eosConfig);
const eosRestAPI = api({
  httpEndpoint: eosHTTPProvider,
  debug: true, // API logging
})

// parse application/json
app.use(bodyParser.json());

// methods
app.post('/transact', (req, res) => {
  const data = req.body; // get body of requests === application/JSON
  if ( data.authKey === undefined || !data.authKey ) {
    throw new Error("Missing authorization key"); // Express will catch this on its own.
  }
  lastKeyProvider = data.authKey;
  const action = {
    actions: [
      {
        account: data.contract,
        name: data.action,
        authorization: [
          {
            actor: data.name,
            permission: 'active'
          }
        ],
        data: data.args
      }
    ]
  }
  eos.transaction(action)
  .then ( result => {
    res.send(result);
  })
  .catch( e => {
    res.send(e);
  });
});

app.post('/balance', (req, res) => {
  const data = req.body;
  if ( data.name === undefined || !data.name ) {
    throw new Error('no name provided');
  }

  eosRestAPI.getCurrencyBalance({
    code: 'eosio.token',
	  account: data.name,
	  symbol: 'ONO'
  })
  .then( result => {
    console.log(result);
    res.send(result);
  })
  .catch( e => {
    res.send(e);
  })
})

app.listen('3000','0.0.0.0', ()=> {
  console.log('server listening on port 3000');
})



